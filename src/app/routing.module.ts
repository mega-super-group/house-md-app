import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DiagnoseComponent} from './components/diagnose.c';
import {LogoutComponent} from './components/logout.c';
import {LoginComponent} from './components/login.c';
import {SignInComponent} from './components/signin.c';
import {DiagnoseListComponent} from './components/diagnose.list.c';

export const routes: Routes = [
    {path: '', redirectTo: 'diagnose', pathMatch: 'full'},
    {path: 'diagnose', component: DiagnoseComponent},
    {path: 'diagnose-list', component: DiagnoseListComponent},
    {path: 'login', component: LoginComponent},
    {path: 'signin', component: SignInComponent},
    {path: 'logout', component: LogoutComponent},
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class RoutingModule {

}
