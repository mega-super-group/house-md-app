import {Component, OnInit} from '@angular/core';
import {ApiService} from '../service/api.service';
import {App} from '../app';
import {Router} from '@angular/router';

@Component({
    selector: 'app-login',
    template: `
        
        <div class="container">

            <div class="row">
                
                <div class="col-md-4"> </div>
	            <div class="col-md-4 welcome-offset">
                
                    <h2 *ngIf="!enableAuthorization"> {{checkingAuthorizationMsg}}</h2>
                    
                    <h3 *ngIf="errorMessage !== null"> {{errorMessage}} </h3>
                    
		            <form *ngIf="enableAuthorization">
               
			            <div class="form-group">
				            <label for="exampleInputEmail1">Email address</label>
				            <input type="email"
                                   class="form-control"
                                   name="form-email"
                                   id="exampleInputEmail1"
                                   [(ngModel)]="email"
                                   aria-describedby="emailHelp" placeholder="Enter email">
				            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
			            </div>
               
			            <div class="form-group">
				            <label for="exampleInputPassword1">Password</label>
				            <input [(ngModel)]="password"
                                   name="form-pass"
                                   type="password"
                                   class="form-control"
                                   id="exampleInputPassword1"
                                   placeholder="Password">
			            </div>
			      
			            <div class="text-center">
			                <button type="submit" (click)="authenticate($event)" class="btn btn-primary">Submit</button>
                        </div>

			            <div class="text-center">
				            <span style="display: block; padding-top: 8px;"> <a routerLink="/signin"> Register as new doctor</a> </span>
			            </div>
               
		            </form>
                
                </div>
	            <div class="col-md-4"> </div>
             
            </div>
            
         
        </div>
        
  `,
    styles: [`
        .welcome-offset {padding-top: 100px}
  `]
})

export class LoginComponent implements OnInit {
    
    public enableAuthorization = false;
    
    public checkingAuthorizationMsg = "One second, checking if you already authorized";
    
    public errorMessage = null;
    
    public email = "";
    public password = "";
    
    constructor(readonly apiService: ApiService, readonly router: Router) {
    
    }
    
    public authenticate(e: Event) {
        
        e.preventDefault();
        
        this.apiService.authorizeCredentials(this.email, this.password).subscribe(user => {
           
           if (typeof user.accessKeys !== "undefined") {
    
               App.setUser(user);
               App.setAuthorized();
               this.router.navigate(["diagnose"]);
               
           }
           
        }, err => {
            
            this.errorMessage = err.error;
            
        });
        
    }
    
    public checkApiAuthorization() {
    
        if (App.tryLoadUser()) {
        
            this.apiService.checkAuthorization(App.getUser().accessKeys[0]).subscribe(a => {
            
                this.router.navigate(["diagnose"]);
            
            }, err => {
            
                this.enableAuthorization = true;
            
            });
        
        } else {
        
            this.enableAuthorization = true;
        
        }
        
    }
    
    ngOnInit(): void {
        
        this.checkApiAuthorization();
        
    }
    
}
