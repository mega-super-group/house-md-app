import {Component, OnInit} from '@angular/core';
import {ApiService, Diagnosis, HealthCondition} from '../service/api.service';
import {App} from '../app';
import {Router} from '@angular/router';

@Component({
    selector: 'app-diagnose',
    template: `
        
        <div class="container">

            <div class="row">
             
	            <div class="col-md-6">
                    
                    <div *ngIf="diagnosis === null" class="card">
	                    <div class="card-body">
		                    <h5 class="card-title">No new patients</h5>
                        </div>
                    </div>
                    
		            <div *ngIf="diagnosis !== null" class="card" style="width: auto;">
			            <div class="card-body">
				            <h5 class="card-title">Patient diagnose</h5>
				            <h6 class="card-subtitle mb-2 text-muted">{{diagnosis?.user.first}} {{diagnosis?.user.last}}</h6>
				            <p class="card-text">{{diagnosis?.description}}</p>
			            </div>
		            </div>
                    
                    
                </div>

	            <div class="col-md-6 text-center">
                    
                    <div class="card" style="overflow-y: scroll; max-height: 570px">
                     
	                    <ul class="list-group">
                      
		                    <li *ngFor="let c of conditions"
                                class="list-group-item"
                                [class]="c.selected ? 'list-group-item list-group-item-primary':'list-group-item'"
                                (click)="selectCondition(c)">
                                {{c.condition.code}} {{c.condition.description}}
                            </li>
                            
	                    </ul>
                     
                    </div>
                    
                </div>
             
            </div>

	        <div *ngIf="error !== null" class="row">

		        <div class="col-md-12 text-right">
			        {{error}}
		        </div>

	        </div>
         
	        <div class="row" style="padding-top: 8px;">

		        <div class="col-md-12 text-right">
                    <button *ngIf="!approving" class="btn-primary btn" (click)="approve()"> Approve </button>
                </div>
                
            </div>
            
        </div>
        
  `,
    styles: [`
        .welcome-offset {padding-top: 100px}
  `]
})

export class DiagnoseComponent implements OnInit {
    
    public enableAuthorization = false;
    
    public errorMessage = null;
    
    public noDiagnosis = false;
    
    public diagnosis: Diagnosis = null;
    
    public error: string = null;
    
    public approving = false;
    
    public conditions: SelectableCondition[] = [];
    
    constructor(readonly apiService: ApiService, readonly router: Router) {
    
    }
    
    public selectCondition(condition: SelectableCondition) {
        
        condition.selected = true;
        
        if (typeof this.diagnosis.conditions === "undefined") {
            this.diagnosis.conditions = [];
        }
        
        const filtered = this.diagnosis.conditions.filter(c => c.id === condition.condition.id);
        
        if (filtered.length > 0) {
           this.diagnosis.conditions = this.diagnosis.conditions.filter(c => c.id !== condition.condition.id);
        } else {
            this.diagnosis.conditions.push(condition.condition);
        }
        
    }
    
    public approve() {
        
        this.approving = true;
        this.error = null;
        
        this.apiService
            .approve(App.getUser().accessKeys[0], this.diagnosis.id, this.diagnosis)
            .subscribe(d => {
                this.refreshData();
            }, err => {
                this.error = err.error;
                this.approving = false;
            });
        
    }
    
    public refreshData() {
        
        for (const cond of this.conditions) {
            cond.selected = false;
        }
        
        this.apiService.nextDiagnosis(App.getUser().accessKeys[0]).subscribe(d => {
            
            this.diagnosis = d;
            this.approving = false;
            
        }, err => {
            
            if (err.status === 404) {
                this.diagnosis = null;
            } else {
                this.error = err.error;
            }
            
        });
        
        this.apiService.listConditions(App.getUser().accessKeys[0]).subscribe(hc => {
            
            this.conditions = hc.map(c => {
                return {
                    condition: c,
                    selected: false
                };
            });
            
        });
        
    }
    
    ngOnInit(): void {
        
        if (App.isAuthorized()) {
            this.refreshData();
        }
        
        App.onAuthorization(() => {
           this.refreshData();
        });

    }
    
}

export interface SelectableCondition {
    
    condition: HealthCondition;
    selected: boolean;
    
}
