import {Component, OnInit} from '@angular/core';
import {ApiService, Diagnosis, HealthCondition} from '../service/api.service';
import {App} from '../app';
import {Router} from '@angular/router';

@Component({
    selector: 'app-diagnose-list',
    template: `
        
        <div class="container">

            <div class="row" style="padding-bottom: 10px" *ngFor="let d of diagnosis">
             
	            <div class="col-md-3">
                
                    {{d?.user.first}}
		            {{d?.user.last}}

	            </div>

	            <div class="col-md-3 text-center">

		            <ul class="list-group">
                        <li class="list-group-item" *ngFor="let c of d?.conditions">
	                        <span class="badge badge-pill badge-light">{{c?.code}}</span>
                        </li>
                    </ul>

	            </div>
             
	            <div class="col-md-6 text-center">
                    
                    <small>{{d?.description | slice:0:128 }}...</small>
                
                </div>
             
            </div>
            
        </div>
        
  `,
    styles: [`
        .welcome-offset {padding-top: 100px}
  `]
})

export class DiagnoseListComponent implements OnInit {
    
    public diagnosis: Diagnosis[] = null;
    
    public approving = false;
    
    public conditions: SelectableCondition[] = [];
    
    constructor(readonly apiService: ApiService, readonly router: Router) {
    
    }
    
    public refreshData() {
    
        this.apiService.listDiagnosis(App.getUser().accessKeys[0]).subscribe(d => {
            this.diagnosis = d;
        });
    
    }
    
    ngOnInit(): void {
        
        if (App.isAuthorized()) {
            this.refreshData();
        }
        
        App.onAuthorization(() => {
            this.refreshData();
        });
        
    }
    
}

export interface SelectableCondition {
    
    condition: HealthCondition;
    selected: boolean;
    
}
