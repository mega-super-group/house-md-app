import {Component, OnInit} from '@angular/core';
import {ApiService, User} from '../service/api.service';
import {App} from '../app';
import {Router} from '@angular/router';
import {first} from 'rxjs/operators';

@Component({
    selector: 'app-sign-in',
    template: `
        
        <div class="container">

	        <div class="row">
		        <div class="col-md-4"> </div>
		        <div class="col-md-4 welcome-offset text-center">
                    <h2> Sign in as doctor </h2>
                </div>
		        <div class="col-md-4"> </div>
            </div>
            
            <div class="row">
                
                <div class="col-md-4"> </div>
	            <div class="col-md-4">

                    <h3 *ngIf="errorMessage !== null"> {{errorMessage}} </h3>
                    
		            <form>
               
			            <div class="form-group">
				            <label for="exampleInputEmail1">Email address</label>
				            <input type="email"
                                   class="form-control"
                                   name="form-email"
                                   id="exampleInputEmail1"
                                   [(ngModel)]="email"
                                   aria-describedby="emailHelp" placeholder="Enter email">
				            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
			            </div>

			            <div class="form-group">
				            <label for="exampleInputName1">First Name</label>
				            <input type="text"
				                   class="form-control"
				                   name="form-first"
				                   id="exampleInputName1"
				                   [(ngModel)]="name"
				                   aria-describedby="nameHelp" placeholder="Enter login name">
			            </div>
               
			            <div class="form-group">
				            <label for="exampleInputPassword1">Password</label>
				            <input [(ngModel)]="password"
                                   name="form-pass"
                                   type="password"
                                   class="form-control"
                                   id="exampleInputPassword1"
                                   placeholder="Password">
			            </div>

			            <div class="form-group">
				            <label for="exampleInputPasswordRepeat1">Password</label>
				            <input [(ngModel)]="passwordRepeat"
				                   name="form-pass-repeat"
				                   type="password"
				                   class="form-control"
				                   id="exampleInputPasswordRepeat1"
				                   placeholder="Repeat password">
			            </div>

			            <div class="form-group">
				            <label for="exampleInputFirst1">First Name</label>
				            <input type="text"
				                   class="form-control"
				                   name="form-first"
				                   id="exampleInputFirst1"
				                   [(ngModel)]="first"
				                   aria-describedby="firstHelp" placeholder="Enter first name">
			            </div>

			            <div class="form-group">
				            <label for="exampleInputLast1">Last Name</label>
				            <input type="text"
				                   class="form-control"
				                   name="form-first"
				                   id="exampleInputLast1"
				                   [(ngModel)]="last"
				                   aria-describedby="firstHelp" placeholder="Enter last name">
			            </div>

			            <div class="form-group">
				            <label for="exampleInputLicence1">LicenceId</label>
				            <input type="text"
				                   class="form-control"
				                   name="form-licence"
				                   id="exampleInputLicence1"
				                   [(ngModel)]="licence"
				                   aria-describedby="firstHelp" placeholder="Enter licence">
			            </div>
			      
                        <div class="text-center">
	                        <button type="submit" (click)="signin($event)" class="btn btn-primary">Submit</button>
                        </div>

			            <div class="text-center">
				            <span style="display: block; padding-top: 8px;"> <a routerLink="/login">Login as the user</a> </span>
			            </div>
			            
		            </form>
                
                </div>
	            <div class="col-md-4"> </div>
             
            </div>
            
         
        </div>
        
  `,
    styles: [`
        .welcome-offset {padding-top: 100px}
  `]
})

export class SignInComponent implements OnInit {
    
    public checkingAuthorizationMsg = "One second, checking if you already authorized";
    
    public errorMessage = null;
    
    public email = "";
    public name = "";
    public password = "";
    public passwordRepeat = "";
    public first = "";
    public last = "";
    public licence = "";
    
    constructor(readonly apiService: ApiService, readonly router: Router) {
    
    }
    
    public signin(e: Event) {
        
        this.errorMessage = null;
        
        e.preventDefault();
        
        if (this.password !== this.passwordRepeat || this.password.length < 3) {
            this.errorMessage = "Password should match both entries and be not less than 3 characters long";
        }
        
        const user: User = {
            email: this.email,
            name: this.name,
            credentials: {
                password: this.password
            },
            first: this.first,
            last: this.last,
            doctorate: {
                licenceId: this.licence
            }
        };
        
        this.apiService.createDoctor(user).subscribe(u => {
            
            if (typeof u.accessKeys !== "undefined") {
                
                App.setUser(u);
                App.setAuthorized();
                this.router.navigate(["diagnose"]);
                
            }
            
        }, err => {
            
            this.errorMessage = err.error;
            
        });
        
    }
    
    ngOnInit(): void {
    
        
    }
    
}
