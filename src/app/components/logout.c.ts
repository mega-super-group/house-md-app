import {Component, OnInit} from '@angular/core';
import {ApiService} from '../service/api.service';
import {App} from '../app';
import {Router} from '@angular/router';

@Component({
    selector: 'app-logout',
    template: `
        
        <div class="container">

            <div class="row">
                
                <div class="col-md-4"> </div>
	            <div class="col-md-4 welcome-offset text-center">

		            Until next journey my friend
                
                </div>
	            <div class="col-md-4"> </div>
             
            </div>
            
         
        </div>
        
  `,
    styles: [`
        .welcome-offset {padding-top: 100px}
  `]
})

export class LogoutComponent implements OnInit {
    
    public errorMessage = null;
    
    constructor(readonly apiService: ApiService, readonly router: Router) {
    
    }
    
    ngOnInit(): void {
        
        this.apiService.deauthorize(App.getUser().accessKeys[0]).subscribe(c => {
           
           App.setUnauthorized();
           
           setTimeout(() => {
               this.router.navigate(["/login"]);
           }, 2000);
           
        });

    }
    
}
