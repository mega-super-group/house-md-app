import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {ApiService} from './service/api.service';
import {HttpClientModule} from '@angular/common/http';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RoutingModule} from './routing.module';
import {LoginComponent} from './components/login.c';
import {DiagnoseComponent} from './components/diagnose.c';
import {LogoutComponent} from './components/logout.c';
import {DiagnoseListComponent} from './components/diagnose.list.c';
import {SignInComponent} from './components/signin.c';

@NgModule({
  declarations: [
    AppComponent, LoginComponent, SignInComponent, DiagnoseComponent, LogoutComponent, DiagnoseListComponent
  ],
  imports: [
    BrowserModule, HttpClientModule, CommonModule, FormsModule, RoutingModule
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule {}
