import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class ApiService {
    
    readonly target: string = null;
    
    constructor(private client: HttpClient) {
        this.target = environment.host;
    }
    
    /**
     * Check if Authorization is valid
     * -----------------------------------------------------------------------
     * @param key
     */
    public checkAuthorization(key: AccessKey): Observable<void> {
        
        return this.client.get<void>(this.target + "/api/v1/authorized", {
            headers: {
                "Authorization": "Bearer " + key.key
            }
        });
        
    }
    
    /**
     * Create new Doctor
     * -----------------------------------------------------------------------
     * @param user User
     */
    public createDoctor(user: User): Observable<User> {
        
        return this.client.post<User>(this.target + "/api/v1/user/doctor", user);
        
    }
    
    /**
     * Authorize some user credentials
     * -----------------------------------------------------------------------
     */
    public authorizeCredentials(email: string, password: string): Observable<User> {
        
        const body: any = {
            email,
            password
        };
    
        return this.client.post<User>(
            this.target + "/api/v1/user/authorize/credentials",
            body
        );
        
    }
    
    /**
     * Deauthorize user
     * -----------------------------------------------------------------------
     * @param key
     */
    public deauthorize(key: AccessKey) {
        
        const url = this.target + "/api/v1/user/deauthorize";
        
        return this.client.delete<void>(url, {
            headers: {
                "Authorization": "Bearer " + key.key
            }
        });
        
    }
    
    /**
     * Take next diagnosis for doctor
     * -----------------------------------------------------------------------
     * @param key
     */
    public nextDiagnosis(key: AccessKey) {
    
        const url = this.target + "/api/v1/diagnosis/next";
    
        return this.client.get<Diagnosis>(url, {
            headers: {
                "Authorization": "Bearer " + key.key
            }
        });
        
    }
    
    /**
     * Finalize some Diagnosis
     * -----------------------------------------------------------------------
     * @param id
     * @param key
     * @param diagnosis
     */
    public approve(key: AccessKey, id: number, diagnosis: Diagnosis) {
        
        const url = this.target + "/api/v1/diagnosis/sign/" + id;
        
        return this.client.put<Diagnosis>(url, diagnosis,{
            headers: {
                "Authorization": "Bearer " + key.key
            }
        });
        
    }
    
    /**
     * Get list of Conditions matched by some name
     * -----------------------------------------------------------------------
     * @param key
     */
    public searchCondition(key: AccessKey, keyword: string) {
    
        const url = this.target + "/api/v1/conditions/search/" + keyword;
    
        return this.client.get<HealthCondition[]>(url, {
            headers: {
                "Authorization": "Bearer " + key.key
            }
        });
        
    }
    
    /**
     * Get list of Health Conditions
     * -----------------------------------------------------------------------
     * @param key
     */
    public listConditions(key: AccessKey) {
        
        const url = this.target + "/api/v1/conditions";
        
        return this.client.get<HealthCondition[]>(url, {
            headers: {
                "Authorization": "Bearer " + key.key
            }
        });
        
    }
    
    /**
     * Take next diagnosis for doctor
     * -----------------------------------------------------------------------
     * @param key
     */
    public listDiagnosis(key: AccessKey) {
        
        const url = this.target + "/api/v1/diagnosis/list";
        
        return this.client.get<Diagnosis[]>(url, {
            headers: {
                "Authorization": "Bearer " + key.key
            }
        });
        
    }
    
}

export interface User {
    id?: number;
    email: string;
    name: string;
    first: string;
    last: string;
    isDoctor?: boolean;
    doctorate?: Doctor;
    credentials?: Credentials;
    accessKeys?: AccessKey[];
    diagnosis?: Diagnosis[];
}

export interface AccessKey {
    key: string;
    createdAt: string;
    expiresAt: number;
}

export interface Doctor {
    id?: number;
    licenceId: string;
    diagnosisRecords?: Diagnosis[];
    user?: User;
    signedRecords?: Diagnosis[];
}

export interface Credentials {
    id?: number;
    password: string;
    user?: User;
}

export interface Diagnosis {
    id?: number;
    description: string;
    status: DiagnosisStatus;
    createdAt?: Date;
    conditions?: HealthCondition[];
    user?: User;
    doctors?: Doctor[];
    signedBy?: Doctor;
}

export interface HealthCondition {
    id?: number;
    code: string;
    description: string;
}

export enum DiagnosisStatus {
    PENDING,
    ASSIGNED,
    INREVIEW,
    SIGNED,
    CLOSED
}
