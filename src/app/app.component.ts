import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ApiService} from './service/api.service';
import {App} from './app';

@Component({
  selector: 'app-root',
  template: `

	  <div class="container nav-container" *ngIf="navigation && authorized">

		  <div class="row">

			  <div class="col-lg-12">

				  <nav class="navbar navbar-expand-md navbar-light bg-faded">

					  <button class="navbar-toggler navbar-toggler-right"
					          type="button" data-toggle="collapse" data-target="#navbarText"
					          aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">

						  <span class="navbar-toggler-icon"></span>
					  </button>

					  <a class="navbar-brand" href="#">
						  House MD
					  </a>

					  <div class="collapse navbar-collapse" id="navbarText">

						  <ul class="navbar-nav ml-1">

							  <li class="nav-item">
								  <a class="nav-link" routerLink="/diagnose" routerLinkActive="active">
									  Diagnosis pane
								  </a>
							  </li>

						  </ul>

						  <ul class="navbar-nav ml-auto">

							  <li class="nav-item">
								  <a class="nav-link" routerLink="/diagnose-list" routerLinkActive="active">
									  Past patients
								  </a>
							  </li>

						  </ul>

						  <ul class="navbar-nav mr-sm-1">

							  <li class="nav-item mr-sm-1">
								  <a class="nav-link" routerLink="/logout">
									  Logout
								  </a>
							  </li>
                          
                          </ul>

					  </div>

				  </nav>

			  </div>

		  </div>

	  </div>
   
	  <div class="container content-container">
		  <router-outlet [hidden]="!authorized"></router-outlet>
	  </div>
  `,
  styles: [`
  
  `]
})
export class AppComponent implements OnInit {
  
    public authorized = false;
    public navigation = false;
    
    constructor(readonly router: Router,
                readonly apiService: ApiService) {
        
    }
    
    public checkApiAuthorization() {
        
        if(App.tryLoadUser()) {
            
            this.apiService.checkAuthorization(App.getUser().accessKeys[0]).subscribe(a => {
                
                this.router.navigate(["diagnose"]);
                App.setAuthorized();
                
            }, err => {
                
                this.router.navigate(["login"]);
                
            });
            
        } else  {
            
            this.router.navigate(["login"]);
            
        }
        
    }
    
    ngOnInit(): void {
        
        App.init();
        
        if(!App.isAuthorized()) {
            this.router.navigate(["login"]);
        }
        
        App.onAuthorization(() => {
            
            this.authorized = true;
            this.navigation = true;
            
        });
        
        App.onLogoutEvent(() => {
            
            this.authorized = false;
            this.navigation = false;
            
        });
        
        this.checkApiAuthorization();
        
    }
    
}
